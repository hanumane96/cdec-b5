terraform {
  backend  {
    bucket = "var.terraform-backend"
    region = "var.region"
    key = "terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
      name = "${var.project}"
      env = var.env
  }
}

resource "aws_subnet" "pri_subnet" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = var.pri_sub_cidr
  availability_zone = var.az1
  tags = {
    Name = "$v[ar.project}-subnet_name" 
    env = var.env
  }
}