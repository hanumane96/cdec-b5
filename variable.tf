variable "region" {
  default = "eu-west-2"
}

variable "terraform-backend"{
  default = "cdec-b11"
}

variable "vpc_name" {
  default = "cbz-vpc" 
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "env" {
  default = "dev"
}

variable "pri_sub_cidr" {
  default = "10.0.0.0/20"
}

variable "pub_sub_cidr" {
  default = "10.0.16.0/20"
}

variable "var.az1" {
  default = "eu-west-2a"
}
 default = "cbz"
}
variable "image_id" {
  default = "ami-030594f92006445fd"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_pair" {
  default = "london-cbz"
}
variable "var.az2" {
  default = "eu-west-2b"
}

variable "project" {
 


##terraform 
