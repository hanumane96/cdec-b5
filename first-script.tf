terraform {
    backend "s3" {
    bucket = "cdec-b11"
    region = "ap-south-1"
    key = "terraform.tfstate"
}
}

provider "aws" {
  region = var.region
}
data "aws_security_group" "my_sg" {
  filter {
    name   = "vpc-id"
    values = ["vpc-00eb81a74f8710cfb"]
}
filter {
    name   = "group-name"
    values = ["default"]
  }
}
resource "aws_instance" "my_instance" {
  ami = "ami-042fab99b38a3963d"
  instance_type = var.instance_type
  key_name = var.key_pair
  tags = var.tags
  vpc_security_group_ids = [data.aws_security_group.my_sg.id]
}

variable "region" {
  description = "aws region"
  default = "eu-west-2"
}

variable "instance_type" {
  description = "instance type"
  default = "t3.micro"
}

variable "key_pair" {
  default = "tf-key"
}

variable "tags" {
  type = map
  default = {
     env = "dev"
     Name = "my-instance"
  }
}

#variable "sg_ids" {
  #type = list
  #default = [
  #"sg-091f8472c95c559a1"
  #]
  #}
output "demo" {
  value = "hello cloud"
}

output "public_ip" {
  value = aws_instance.my_instance.public_ip
}